# Softcore Datapack

This is a datapack designed for multiplayer hardcore minecraft, introducing challenges that can be used to revive all dead players.

Challenges can be completed once per player. Once completed, return to the death location of any dead player to redeem the revival.

Challenges are visible to all players in the achievements menu once a player has died.

## Install

1. Download the latest datapack file (the one under *Other*) from [the latest release](https://gitlab.com/zsturgess/softcore-minecraft-datapack/-/releases/permalink/latest)
1. Install the datapack as per [this guide on the minecraft wiki](https://minecraft.fandom.com/wiki/Tutorials/Installing_a_data_pack)
1. As an op, run `/function #sc_core:setup`
