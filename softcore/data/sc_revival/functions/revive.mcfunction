execute as @p[scores={sc_challengesPassed=1..}] run scoreboard players remove @s sc_challengesPassed 1
execute as @a[gamemode=spectator] run tp @s ~ ~1 ~
gamemode survival @a[gamemode=spectator]
scoreboard players reset @a sc_challengesPassed
advancement revoke @a only sc_challenges:root
fill ~-1 ~-1 ~-1 ~1 ~-1 ~1 infested_mossy_stone_bricks
setblock ~ ~1 ~ air
setblock ~ ~ ~ minecraft:air